let string3 = (date)=>
{  
    let monthObj = {
        1:"January",2:"Febuary",3:"March",4:"April",5:"May",6:"June",7:"July",8:"August",
        9:"September",10:"October",11:"November",12:"December"
    }
    let dateArr = []
    dateArr.push( date.split("/"))
    
    let monthArray =[]
    monthArray= dateArr[0].map((a)=>parseInt(a))
  
    let month = monthArray[1]
    if(month<1||month>12||Number.isNaN(month)==true)
    {
        return []
    }
    for(let [key,value] of Object.entries(monthObj))
    {

       if(`${key}`==month)
       {
        return `${value}`
       }

    }

}
module.exports = string3
